from kmtools.structure_tools import DomainTarget
from tkpod.plugins.modeller import Modeller


structure_url = "2j4j.cif"
target = DomainTarget(
    model_id=0,
    chain_id='D',
    template_sequence='MNIILKISGKFFDEDNVDNLIVLRQSIKELADNGFRVGIVTGGGSTARRYIKLAREI-----G---IGEAYLDLLGI-WASRLNAYLVMFSLQ-------------------------------DLAYMHVPQ--SLEEFIQDWSHGKVVVTGGFQPGQ',
    template_start=1,
    template_end=117,
    target_sequence='KVVVVKYGGNAMTDDT--LRRAFAADMAFLRNCGIHPVVVHGGGPQITAMLRRLGIEGDFKGGFRVTTPEVLDVARMVLFGQVGRE-LVNLINAHGPYAVGITGEDAQLFTAVRRSVTVDGVATDIGLVGDVDQVNTAAMLDLVAAGRIPVVSTLAPDA',
)

modeller_data = Modeller.build(structure_url, use_auth_id=True, bioassembly_id=False, use_strict_alignment=False)
structure_bm, results = Modeller.create_model([target], modeller_data)

