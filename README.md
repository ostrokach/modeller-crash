# Modeller Crash

To reproduce, run:

```bash
# Create conda environment
conda env create -n modeller-crash -f environment.yaml
source activate modeller-crash

# Run test
python -X faulthandler demo.py
```

